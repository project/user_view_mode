# User View Mode

This module allow you to choose default view mode display for the user depending on his role.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/user_view_mode).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/user_view_mode).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

No special requirements.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Enable the module from extend.
2. Edit or create a role and set your view mode.

  
## Maintainers

- Mohammad Abdul-Qader - [m.abdulqader](https://www.drupal.org/u/imalabya)
- Yahya Al Hamad - [YahyaAlHamad](https://www.drupal.org/u/yahyaalhamad)
- Saif Bataineh - [SyfJO](https://www.drupal.org/u/syfjo)
